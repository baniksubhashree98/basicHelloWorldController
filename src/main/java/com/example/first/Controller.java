package com.example.first;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
//    the value will be fetched from the application.properties file and attached to the variable welcomeMessage
    @Value("${welcome.message}")
    private String welcomeMessage;

    @GetMapping("/hello")
    public String helloWorld() {
        return welcomeMessage;
    }
}


