package com.example.first;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest(classes = FirstApplication.class)
@AutoConfigureMockMvc
//@WithMockUser
public class ControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldRetrieveStringHelloWorld() throws Exception {
        String expectedString = "Hello World";

//        String actualString = mockMvc.perform(get("/hello")).andReturn().getResponse().getContentAsString();

        MvcResult mvcResult = mockMvc.perform(get("/hello")).andReturn();
        System.out.println(mvcResult);
//        System.out.println(mvcResult.getRequest());

        String actualString = mvcResult.getResponse().getContentAsString();

        assertEquals(expectedString, actualString);
    }
}
